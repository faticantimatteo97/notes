# VPN

- VPN principles
- SSL Tunneling
- VPN device placement
- IPsec

## Virtual Private Networks

- Definition (NIST SP800-113): A virtual network, built on top of an existing network infrastructure, which can provide a secure communications mechanism for data and other information transferred between two endpoints
- Typically based on the use of encryption, but several possible choiches for:
	- How and where to perform encryption
	- Which parts of communication should be encrypted
- Important subsidiary goal: usability
	- If a solution is too difficult to use, it will not be used ⟶  poor usability leads to no security

### Security Goals for a VPN

- Traditional
	- Confidentiality of data, impossible to understand the content of the data to peaple that are not authorized to access
	- Integrity of data, need to provide a way to guarantee that the data are not tampered with
	- Peer Authentication, need to have some kind of certainty about the identity of the communicating peers
- Extended
	- Replay Protection, Possible to avoid the reusage of a package in the network
	- Access Control, Restriction of who can use the vpn
	- Traffic Analysis Protection

### Usability goals

- Transparency
	- VPN should be invisible to users, software, hardware
- Flexibility
	- VPN can be used between users, applications, hosts, sites
- Simplicity
	- VPN can be actually used

### Site-to-site security

![vpn image](https://www.synology.com/_images/autogen/How_to_set_up_Site_to_Site_VPN_between_Synology_Router_and_UniFi_SG/1.png)
### Host-to-site security

Single host that virtually join one of the sites of our organization

## Physical layer

Protection of the data on a special wire, happen only on devices physically connected.

- Confidentially: on cable
- Integrity: on cable
- Authentication: none
- Replay protection: none
- Traffic analysis protection: on cable
- Access control: physical access
- Transparency: full transparency
- Flexibility: can be hard to add new sites
- Simplicity: excellent!

## Datalink layer: protect a single link

- Confidentiality: on link (”virtual
cable”)
- Integrity: on link
- Authentication: none
- Replay protection: none
- Traffic analysis protection: on link
- Access control: physical access
- Transparency: full transparency
- Flexibility: can be hard to add new sites
- Simplicity: excellent!

## Network layer: protect end-to-end between systems

- Confidentiality: between hosts/sites
- Integrity: between hosts/sites
- Authentication: for host or site
- Replay protection: between hosts/sites
- Traffic analysis protection: host/site information exposed
- Access control: to host/site
- Transparency user and SW transparency possible
- Flexibility: may need HW or SW modifications
- Simplicity: good for site-to-site, not good for host-to-site

## Transport layer: protection end-to-end between processes

- Confidentiality: between apps/hosts/sites
- Integrity: between apps/hosts/sites
- Authentication: for user, host, site
- Replay protection: between apps/hosts/sites
- Traffic analysis protection: protocol/host/site info. exposed
- Access control: user/host/site
- Transparency user and SW transparency possible
- Flexibility: HW or SW modifications
- Simplicity: good for site-to-site, not good for host-to-site

## Application layer: Security for a single application

- Confidentiality: between users/apps
- Integrity: between users/apps
- Authentication: user
- Replay protection: between apps
- Traffic analysis protection: all but data exposed
- Access control: only data access secured
- Transparency: only user transparency
- Flexibility: SW modifications
- Simplicity depends on application

### VPN: then?

- It looks best to introduce security in the
	- Transport layer
	- Network layer
- These are the most popular choices for VPNs
- Other options:
	- Secure Application layer protocols: obly protect a single application, but are often used for specialized purposes, e.g. S/MIME or PGP for secure e-mail
	- Secure Data Link layer protocols: are mostly used with PPP or other modem-based communication e.g. PPTP,L2TP,LTF

## Tunneling

- Operation of a network connection on top of another network connection
- It allows two hosts or sites to communicate through another network that they do not want to use directly

![tunneling image](https://img.favpng.com/0/23/13/juniper-networks-virtual-private-network-ipsec-tunneling-protocol-ssl-vpn-png-favpng-BgJvPcdYytUuZeScmAnvWapgj.jpg)
### Site-to-site tunneling

- Enables a PDU to be transported from one site to another without its contents being processed by hosts on the route

- Idea: Encapsulate the whole PDU in another PDU sent out on the network connecting the two sites
	- Encaptsulation takes place in edge router on src.site
	- Decapsulation takes place in edge router on dst.site
- Note that the host-to-host communication does not need to use IP
 
### Secure tunneling

- Enables a PDU to be transported from one site to another without its contents being seen or changed by hosts on the route
- Idea: Encrypt the PDU, and then encapsulate it in another PDU sent out on the network connecting the two sites
	- Encryption can take place in edge router on src.site
	- Decryption can take place in edge router on dst.site
- Note: dst.address in IP header is for dst.edge router

### Tunneling for VPN's

- Tunneling offers the basic method for providing a VPN
- Where in the network architecture to initiate and terminate the tunnel:
	- Router/firewall?
	- Special box?
	- Host?
	- Application?
- Which layer to do the tunneling in:
	- Transport layer?
	- Transport layer?
- Other possibilities
- And of course: Is tunneling the only possible technique?

## Secure Socket Layer

- SSL 3.0 has become TLS standard (RFC 2246) with small changes
- Applies security in the Transport layer
- Originally designed (by Netscape) to offer security for client-server sessions
- If implemented on boundary routers (or proxies) can provide a tunnel between two sites - typically LANs
- Placed on top of TCP, so no need to change TCP/IP stack or OS
- Provides secure channel (byte stream)
	- Any TCP-based protocol
	- https:// URls, port 443
	- NNTP, SIP, SMTP
- Optional server authentication with public key certificates
	- Common on commercial sites

### HTTPS (http on top of TSL)

![tsl image](https://www.cloudflare.com/img/learn/tls/tls-1.2-vs-1.3-a.png)

### SSL protocol Architecture

- Adds extra layer between T- and A-layers, and extra elements to A-layer
- Record Protocol: Protocol offering basic encryption and integrity services to applications
- Application Protocols: control operation of the record protocol
	- Handshake: Used to authenticate server (and optionally client) and to agree on encryption
keys and algorithms
	- Change cipher spec: Selects agreed keys and encryption algorithm until further notice
	- Alert: Transfers information about failures

![SSL image](https://www.vialattea.net/esperti/inform/ssl/arch.png)

### SSL/TLS Record protocol

- Offers to applu the following steps to PDUs:
	1. Fragmentation into blocks od <= 2^14 bytes
	2. (optional) Lossless compression
	3. Addition of a keyed MAC, using a shared secret MAC key
	4. Encryption, using a share secret encryption key
	5. Addition of header indicating Application protocol in use

![SSL Record image](https://keshavdulal.github.io/bscit-network-security-notes/images/16/fig-16.3.png)

### SSL Handshake

1. Hello phase
2. Server authentication
3. Client authentication
4. Finish

![SSL handhake](https://www.researchgate.net/profile/Sirikarn-Pukkawanna-2/publication/270574368/figure/fig1/AS:614251328716812@1523460337954/Steps-of-the-SSL-handshake-and-messages.png)
### SSL/TLS Security Capabilities

- Conventionally expressed by a descriptive string, specifying:
	- Version of SSL/TLS
	- Key exchange algorithm
	- Grade of encryption (previous to TLSv1.1)
	- Encryption algorithm
	- Mode of block encryption (if block cipher used)
	- Cryptographic checksum algorithm
- Example: TLS_RSA_WITH_AES_128_CBC_SHA
	- TLS (Latest version of) TLS → poor
	- RSA RSA key exchange → poor
	- WITH (merely filler...) → poor
	- AES_128 128-bit AES encryption → poor
	- CBC Cipher Block Chaining → poor
	- SHA Use HMAC-SHA digest

### Key echange and authentication

- Possible ways of agreeing on secrets in TLS are:
	- RSA: RSA key exch. (secret encrypted with recipient’s publ. key)
	- DHE RSA: Ephemeral Diffie-Hellman with RSA signatures
	- DHE DSS: Ephemeral Diffie-Hellman with DSS signatures
	- DH DSS: Diffie-Hellman with DSS certificates
	- DH RSA: Diffie-Hellman with RSA certificates
	- DH anon: Anonymous Diffie-Hellman (no authentication)
	- NULL No key exch.
- Variant: If followed by “EXPORT_”, weak encryption is used. (This option only
available prior to TLSv1.1)
- Note: “Key exchange” only establishes a pre-secret! From this, a master secret is
derived by a pseudo-random function (PRF). Shared secret encryption key is
derived by expansion of master secret with another PRF. (In TLS several keys are
derived for different purposes.)

### SSL/TLS Heartbeat

- It is an extention (RFC 6520) that allows to keep an established session alive
	- That is, as soon as the data exchange between two endpoints terminates, the session will also terminate
- To avoid the re-negotiation of the security parameters for establishing a secure session, we can keep using the same parameters even
if there is no exchange of data
- It introduce two messages: **HeartbeatRequest** and **HeartbeatResponse**

### Heartbeat exchange

- When one endpoint sends a HeatbeatRequest message to the other endpoints, the former also starts what is known as the **retransmit timer**
	- During the time interval of the retransmit timer, the sending endpoint will not send another HeartbeatRequest message
- An SSL/TSL session is considered to have terminated in the absence of a HeartbeatResponse packet within a time interval

### Heartbeat payload

- As a protection against a replay attack, HeartbeatRequest packets include a payload that must be returned without change by the receiver in its HeartbeatResponse packet
- The Heartbeat message is defined as
 
 ```
 struct {
 	HeartbeatMessageType type;
	uint16 payload_length;
	opaque payload[HeartbeatMessage.payload_length];
	opaque padding[padding_length];
} HeartbeatMessage;
 ```

### Heartbleed bug

- Bug in OpenSSL library (4/4/2014)
- The receiver of request did not check that the **size of the payload in the packet** actually equaled the **value given** by
the sender to the payload length field in the request packet
	- The attacker sends little data but sets the size to max
	- The receiver allcates that amount of memory for the response and copied max bytes from the mem location where the request
packet was received
	- Then, the actual payload returned could potentially include objects in the memory that had nothing to do with the received payload
		- Objects could be private keys, passwords, and such...

## SSL VPN Architecture

Two primary models:
	1. SSL Portal VPN
		- Allow remote users to:
			- Connect to VPN gateway from a Web browser
			- Access services from Web site provided on gateway
	2. SSL Tunnel VPN
		- Allow remote users to:
			- Access network protected by VPN gateway from
			- Web browser allowing active content
		- More capabilities than portal VPNs, as easier to provide more services

### SSL VPN functionalities

Most SSL VPNs offer one or more core functionalities:
- Proxying: Intermediate device appears as true server to client. E.g. Web proxy.
- Application Translation: Conversion of information from one protocol to another.
	- e.g. Portal VPN offers translation for applications which are not Web-enabled, 
	so users can use Web browser to access applications with no Web interface.
- Network Extension: Provision of partial or complete network access to remote users, typically via Tunnel VPN.
	- Two variants:
		- Full tunneling: All network traffic goes through tunnel.
		- Split tunneling: Organisation’s traffic goes through

