# Link-local attacks

### Topics

- Network eavesdropping
- ARP poisoning
- IPv6 Neighbor Discovery threats
- IPv6 Rougue RA (RA spoofing)
- Rougue DHCP

## Network sniffing (Network eavesdropping)

- Capturing packets from the network transmitted by others nodes and reading the data content in search of sensitive information
	- passwords, session tokens, or alike
- Done by using tools called network sniffers (or protocol analyzers)
	- Huge list of tools: not exhaustive list includes Ettercap, bettercap, networkminer, driftnet, dsniff, macof...
- Analyze the collected data like protocol decoders or stram reassembling
- Work in passive mode
	- packets are simply captured, copied, and passed at user level for further analysis
- Requires to be along the path or a broadcasting domain

### Realize network sniffing

- Network interface in promiscuous mode
- Sniffer must be along the path or, at least, in the same network
	- Non-switched LAN (LAN with HUBs)
		- the ideal case beacause the hub duplicates every frame to all ports
	- LAN with switches
		- breaking switch segmentation, sometimes by flooding the switch with a large amout of frames (MAC flooding)
		- performing arp spoof attack to redirect the traffic from one port to another
			- possible Man-In-The-Middle attack
	- wireless LAN
		- possible if no encryption is used or weak  encryption is used (scenario becomes equivalent to LAN with HUBs)
		
### Breaking the switch segmentation mechanism

- Flashback: bridge and switch
- Bridges: first way to reduce collisions and segment a network.
	- Have two ports joining to network segments
	- Only frames supposed to go on the other segment of the network are replicated (filtering)
	- "store & forward": read and regenerate a frame only if needed
- Switches: multiport bridges
	- Regenerate a frame only in the segment of the destination
	- Learn the host in each network segment in real time

### Mac Addreess/CAM Table Review

- CAM Table stands for Content Addressable Memory
- The CAM Table stores information such as MAC addresses available on physical ports with their associated VLAN parameters
- CAM Tables have a fixed size
- As frames move in the switches, the CAM is filled with the MAC addresses
	- Ex: source MAC address are associated with the related port
- If a MAC is unknown, it is replicated on ALL the ports -> flood

### CAM overflow

- Theoretical attack until May 1999 
  - macof tool since May 1999
  - About 100 lines of perl from Ian Viteck
  - Later ported to C by Dug Song for "dsniff"
- Based on CAM Table's limited size
- Usually switches use hash to place MAC in CAM table
	- like hashed list, where buckets can keep a limited numbers of values
	- If the value is the same there are n buckets to place CAM entries, if all n are filled the packet is flooded

### What happen next?

- Many outcomes
	- Switch starts flooding (attack success)
	- Switch freezes (denial of service)
	- Switch crash (denial of service)
- Today not really effective: port security in switches
	- Allows you to specify MAC addresses for each port, or to learn a certain number of MAC addresses per port
	- Upon detection of an invalid MAC the switch can be configured to block only the offending MAC or just shut down the port

## ARP Apoofing

### ARP

- An ARP request message should be placed in a frame and broadcast to all computers on the network
- Each computer receives the request and examines the IP address
- The computer mentioned in the request sends a response; all other computers process and discard the request without sending a response

### ARP table

- Dynamic table that holds the IP-MAC pairings
- It is accessed before sendig any Ethernet frame
- It starts empty and is filled as the MAC addresses are collected
- Unused MAC addresses are removed after a timeout (address ageing) in the order of minutes
- According to RFC 826 (ARP), when receiving an ARP reply, the IP-MAC pairing is updated (age and pairing...)
