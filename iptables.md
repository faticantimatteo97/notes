## Resurces

- [ ] www.frozentux.net/iptables-tutorial/iptables-tutorial.html
- [ ] [local network protection for ipv6](https://tools.ietf.org/html/rfc4864)

## Ip tables

It is the implementation of a packet filtering firewall for Linux that runs in kernel space
	- evolution of ipchains and ipfw. Coming successor will be nftables

Iptables tool inserts and deletes rules from the kernel's packet filtering table and it can also operate at the Transport layer (TCP/UDP)

### Iptables fundamentals

- The rules are grouped in **tables**
- Each table ha different **CHAINS** of rules
- Each packet is subject to each rule of a table
- Packet fates depend on **the first matching rule**
- To see chains and rules of the filter table:
	- ``` iptables -L ```
	- or (better) ``` iptables -L -n -v --line-numbers ```

## Filter table

- Three built-in rule chains:
	- INPUT
	- OUTPUT
	- FORWARD
- If a packet reaches the end of a chain, then is the chain policy to determine the fate of the packet (DROP/ACCEPT)

### Examples

``` 
iptables -A input -p icmp –icmp-type echo-request -j DROP
iptables -A input -p tcp –-destination-port 80 -j ACCEPT
iptables -A input -j REJECT
```

Different *targets*:
- **ACCEPT:** the packet is handed over to the end of application or the operating system for processing
- **DROP:** the packet is blocked
- **REJECT:** the packet is blocked, but it also sends an error message to the source host of the blocked packet
	- ``` --reject-with <qualifier> <qualifier> is an ICMP message ```
- **LOG:** the packet is sent to the syslog daemon for logging
	- *iptables* continues processing with the next rule in the table
	- You can't log and drop at the same time ⟶ use two rules

Allow both port 80 and 443 for the webserver on inside:

```
iptables -A FORWARD -s 0/0 -i eth0 -d 192.168.1.58 -o eth1 -p TCP \
--sport 1024:65535 -m multiport --dport 80,443 -j ACCEPT
```

The return traffic from webbserver is allowed, but only of sessions are
established:

```
iptables -A FORWARD -d 0/0 -o eth0 -s 192.168.1.58 -i eth1 -p TCP \
-m state --state ESTABLISHED -j ACCEPT
```

If sessions are used, you can reduce an attack called half open
- Half open is known to consume server all free sockets (tcp stack memory) and is
senced as a denial of service attack, but it is not.
Sessions are usally waiting 3 minutes.


## Built-in tables

1. MANGLE: manipulate bits in TCP header
2. FILTER: packet filtering
3. NAT: network address translation
4. RAW: exceptions to connection tracking
	- When present RAW table has the highest priority
	- Used only for specific reasons
	- Default: not loaded

### MANGLE table

- Used for IP header manipulation (like ToS,TTL,..)
	- should not be used for FILTERING
	- should not be used for NAT
- Five chains to alter:
	- PREROUTING: incoming packets before a routing decision
	- INPUT: packets coming into the machine itself
	- OUTPUT: locally generated outgoing packets
	- FORWARD: packets being routed through the machine
	- POSTROUTING: packets immediately after the routing decision

### NAT table

- Used for NAT (Network Address Translation): to translate the packet's source field or destination field
	- Only the first packet in a stream will hit this table (the rest of the packets will automatically have the same action)
- Special targets (*packet fates/actions):
	- DNAT: destination nat
	- SNAT: source nat
	- MASQUERADE: dynamic nat (when fw interface address is dynamically assigned)
	- REDIRECT: redirects the packet to the machine itself

### NAT'ing targets

- DNAT: Destination address traslation
	- Transform the destination IP of incoming packets
	- Used in PREROUTING chain
- SNAT: Source address translation
	- Transform the source IP of outgoing packets
		- can be done one-to-one or many-to-one
	- Used in POSTROUTING chain
- MASQUERADE: like SNAT but the source IP is taken from the dynamicallly assigned address of the interface

### Chain and tables priorities

![chain image](https://i.stack.imgur.com/4wdkF.png)

### User defined chains

It is possible to specify a jump rule to a different chain within the same table
- The new chain must be user specified
- if the endo of the user specified chain is reached, the packet is sent back to the invoking chain

## iptables logging

- LOG as possible target
	- "non-terminating target" i.e. rule traversal continues at the next rule
	- to log dropped packets, use the same DROP rule, but with LOG target
- When this option is set for a rule, the Linux kernel will print some information on all matching packets (like most IP header fields) 
via the kernel log (where it can be read with dmesg or syslogd((8))
	- --log-level level: specifies the type of log (emerg, alert, crit, err, warning, notice, info, debug)
	- --log-prefix prefix: add further information to the front of all messages produced by the logging action

### Log example

- Log forwarded packets
	- ``` iptables -A FORWARD -p tcp -j LOG \ --log-level info --log-prefix "Forward INFO" ```
- Log and drop invalid packets
	- ``` iptables -A INPUT -m conntrack --ctstate \ INVALID -j LOG --log-prefix "Invalid packet" ```
	- ``` iptables -A INPUT -m conntrack --ctstate \ INVALID -j DROP ```

