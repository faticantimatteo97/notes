# Network Traffic Regulation with Firewalls

## Traffic regulation

We want to make some decision on the traffic that we will exchange

- A router is a device that connect two networks
- A firewall is a device that besides acting as a router, also contains (and implements) rules 
- to determine whether packets are allowed to travel from one network to another

> Routers can do some controls on the traffic using ACL (access control list) but are very limited

### Why firewalls?

- The reason is that we want to restrict the acces from outside to our network
	- internet is used by millions of people -> unsafe
- Prevents attackers from gettin too close
	- confinment of the attackers
- Restricts people from leaving
	- controlling the traffic in both ways (coming inside - going outside)

### Secure traffic regulation

Implement a set of policy(how the system should work)

- To attain a certal level of network security, you can:
	- Regulate wich traffic is allowed (sources,destinations,services...)
	- Protect the traffic by encryption
	- Monitor the traffic for "bad behaviour"
	- Monitor the hosts for "bad behaviour"

### Firewall Design & Architecture Issue

- **Least privilege:** we want to give as few as possible privilege to realize the task
- **Defense in depth:** we want several layers of security
- **Choke point:** a single point where everything can be controlled in a precise way
- **Weakest links:** system is weak as his weakest link, secure the weakest points
- **Fail-safe stance:** if we have some trubles we prefer a denial of service instead an insecure service
- **Universal participation:** means that all the systems should take part in this mechanism of protection
- **Diversity of defence:** different tipes of defense 
- **Simplicity:** don't have mechanism too much complicated, difficoult to mantain

## Host base packet filter

Kind of firewall that disciplines the traffic in/out of a single host, also called "personal firewall"
- It specifies the packets that can be received and sent
- Vedor products generally work-per-app: eache installed application has a known policy that has to obey

![ACL image](https://www.codexsprawl.com/wp-content/uploads/2016/11/fire0401-e1479160422293.gif)

### Network Acces Control Lists

- List the rights for accessing/using networks
	- Extesively used in switches, routers and firewalls
- Usually distinguish between incoming and outgoing traffic, per interface/port
	- Ex: lists of IP addresses that can send packets to an interface/port
- Stateless: every packet is treated independently, without any knowledge of what has come before

### Dual-homed host

![Dual-homed host image](https://www.cs.ait.ac.th/~on/O/oreilly/tcpip/firewall/figs/fire0403.gif)

### Bastion host

Hardened computer used to deal with all traffic coming to a protected network from outside
- Hardening is the task of reducing or removing vulnerabilities in a computer system:
	- Shutting down unused or dangerouse services
	- Strengthening access controls on vital files
	- Removing unnecessary accounts and permissions
	- Using stricter configuration for vulnerable components, such as DNS, sendmail, FTP, Apache, Tomcat...
- Specially suitable for use as Application Proxy Gateways
	- Before allowing the traffic to go in we can check the packet content and perform the request as an intermediary 
that not simply forward packets but can generate safer packets

### DMZ

- Demilitarized zone
	- Computer host or small network inserted as a neutral zone between a company's private network and the outside public network
	- Network construct that provides secure segregation of networks that host services for users, visitors, or partners
- DMZ use has become a necessary method of providing a multilayered, defense-in-depth approach to security
- Reduce and regulate the access to internal components of the IT system

### Defense in depth

A security approache in which IT systems are protected using multiple **overlapping** systems
- Add redundancy to the defensive measures
- Aim to remove the single point of failure
- Find the right balance between complexity and multiplicity of defence measures
In order to compromise the system, an attacker has to find multiple vulnerabilities, in different components

![DMZ image](https://docstore.mik.ua/orelly/networking_2ndEd/fire/figs/fire2.0603.gif)

### Screened Subnet Using two Routers/Firewalls

![screened subnet image](https://docstore.mik.ua/orelly/networking_2ndEd/fire/figs/fire2.0604.gif)

### DMZ to segment the network

![DMZ segment image](https://www.spamtitan.com/web-filtering/wp-content/uploads/2018/03/perimiter_security2.jpg)

### Split DMZ

![split DMZ image](https://docs.oracle.com/cd/E37670_01/E36387/html/images/deplarch2.png)

## Packet filters (stateless firewall)

- Drop packet based on their source or destination addresses or port numbers or flags
- No context, only contents (only the header)
- Can operate on:
	- incoming interface
	- outgoing interface
	- both
- Check packets with fake IP addresses:
	- from outside ("ingress filtering")
	- from inside ("egress filtering")

Packet filters operate at Transport, Network and Data Link layers

![Packet filter example](https://files.transtutors.com/book/qimg/cdc91f1f-3c83-465b-9e52-91b4292a63b7.png)

### How to check the direction of TCP?

direction can be determined considering TCP flags, in particolar the SYN

### Problems with Packet Filters

- Only a small number of parameters
	- easy to specify filtering rules which are too specific or too general
- Payload of TCP packet is not inspected
	- no protection against attacks based on upper-layer vulnerabilities
- Limited logging ability (restricted to the few parameters used by the filter)
- No authentication facilities
- Susceptible to attack based on vulnerabilities in various implementation of TCP and/or IP

### Incoming TCP connections with IP frag 

- Firewall blocks any incoming TCP connection
- ACK packet is allowed for outgoing packets
- Internal host reassembles a packet with the SYN bit set because two fragment offsets are chosen in order to set the SYN bit
-  Attacks
	- SYN scan
	- Create TCP connection
	- SYN flood - DoS

## Stateful firewall

### Stateful packet inspection

Stateful Inspection Firewalls (or Dynamic Packet Filters) can keep track of established connections
- Can drop packet based on their source destination IP addresses, port numbers and possibly TCP flags
	- solve one major problem of simple packet filters, since they can check that incoming traffic for a high-numbered
port is a genuine response to a previous outgoing request to set up a connection

### Connection tracking

Considered TCP States
- Setting up connection:
	- client calls from (high-numbered) port to port for application on server
	- server replies to (high-numbered) port on client
	- connection is considered established when the server gives correct SYN/ACK response
- Closing connection:
	- both parties have to close the connection by sending a TCP packet with FIN flag set before the connection is considered closed

![TCP states image](https://www.ictshore.com/wp-content/uploads/2016/12/1017-01-TCP_States_diagram.png)

Stateful firewall tracking applicable even for UDP connection (stateless).

![UDP tracking image](https://docstore.mik.ua/orelly/networking_2ndEd/fire/figs/fire2.0802.gif)

### Application-level filtering (proxy-like)

- Deal with the details of services
- Need a separate special-purpose mechanism for each application
	- Example: mail filters, FTP, HTTP proxy
	- Big overhead, but can log and audit all activity
- Can support user-to-gateway authentication
	- Log into the proxy server with username and password
- Pro:
	- Logging capacity
	- intelligent filtering
	- User-level authentication
	- Protection from wrong implementation
- Cons:
	- Can introduce lag
	- Application-specific
	- Not always transparent


### Host based firewalls

- A firewall on each individual host to protect that one machine
- Selectively enable specific servics and ports that will be used to send and receive traffic
- A host-based firewall plays a big part  in:
	- reducing what is accessible to an outside attacker
	- protecting the other elements of the IT system if one of the component (ex, a process) is compromised

### TCP Relay

- Splices and relays TCP connctions
	- Does not examine the contents of TCP segments
	- Can use ACL
	- Less control than application-level gateway
- Client applications must be adapted for SOCKS
	- Universal interface to cicuit-level gateways
- Example:  ``` ssh -D 12345 <remote_host>  ``` (proxy toggle used on the example)

![tcp relay image](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcROZhu43Kw1JXKNVhUxEdIvkNX8ekqRnRgJ7TxBEK8vYLSbe9tc3ghRo2I1kJWI-KCC860&usqp=CAU)


### Application vs circuit level gateway

- **Application gateway**:
	- incoming packet ⟶ client application is verified ⟶ User is authenticated
- **Circuit level gateway**:
	- incoming packet ⟶ User logon in authenticated ⟶ Virtual Circuit is established

### Common firewall weaknesses

- No content inspection causes the problems
	- Software weakness (buffer overflow, SQL injection exploits)
	- Protocol weakness (WEP in 802.11)
- No defense against:
	- Denial of service
	- Insider attacks
- Firewall failures has to be prevented
	- Firewall cluster for redundancy

### NG-Firewalls

Next Generation firewalls try to include not only traffic filtering but also additional features:
- Intrusion Detection System
- VPN gateway
- Deep Packet Inspection
- Traffic shaping

## Summary

- Traffic regulation: routers and firewall
	- Decide the packets that can pass through the node
- Firewall architectures: where they go in the network?
	- Network segmentation and DMZ
- Types of firewall:
	- Host firewall, stateless, stateful, application-gateway, circuit-gateway

- Stateless firewall weaknesses
	- No state, IP fragmentation

















