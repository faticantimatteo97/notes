# Cloud Storage

- Data storage in the cloud
- Basic concepts
	- Storage models (Cell and journal storage)
	- Atomicity
	- File systems
- High performance file system
	- Network and Parallel file systems
	- Google and Hadoop file systems
- NoSQL datastore
	- Google Big Table
	- Amazon Dynamo  
---

- [ ] Cloud Computing Theory and Pratice, Sections 2.10, Chapter 8
- [ ] Hadoop DFS - https://www.edureka.co/blog/apache-hadoop-hdfs-architecture/
- [ ] readings on BigTable, Dynamo

## Data storage in the Cloud

- There are various form of data storage in cloud systems
	- Distributed file systems
		- Google File System (GFS), Hadoop Distributed File System (HDFS)
		- GFS and HDFS rely on concept early developed in Network File Systems and Parallel File Systems
	- NoSQL database (or NoSQL datastore or simply datastore)
		- E.g. Cassandra, MongoDB
	- Key-value storage systems
		- E.g. BigTable, Dynamo
- Common goals
	- Massive scaling on demand
	- High availability
	- Simplified application development and deployment

## Basic Concepts

### Atomicity

- Multi-step operation (transaction) should complete without any interruption (atomic)
- Atomicity requires HW support, i.e. non-iterruptable ISA operations
	- Test-and-set: writes to a memory location and returnss the old content of that memory cell as non-interruptible operations
	- Compare-and-swap: compares the contents of a memory location to a given value and, only if the two values are the same, modifies the contents of that memory location to a given new value
- Atomicity require mechanism to access shared resources
	- Locks, semaphore, monitors
	- Allow to create critical section

### Two type of atomicity

- all-or-nothing
- before-or-after

### All-or-nothing atomicity

- Two phases
	- pre-commit phase; preparatory actions, can be undone
		- allocation od a resource, fetching a page from secondary storage, allocation of memory on the stack
	- commit-point
	- post-commit phase/commit step; irreversible actions
		- alteration of the only copy of an object
	- To manage failures and ensure consistency we need to maintain the history of all the activities	
		- Logs are necessary

### Before-or-after atomicity

- Cuncurrent action have the before-or-after property if their effect from the point of view of their invokers is as if the action occurred either completely before or completely after one another
- Before-or-after atomicity:
	- the result of every trad or write is the same as if that read or write occurred either completely before or completely after any other read or write

## Storage models & desiderated properties

- Physical storage
	- A local disk, a removable USB disk, a disk accessible via a network
	- Solid state or magnetic
- A *storage model* describes the layout of a data structure in physical storage
	- cell storage
	- journal storage
- Desidered properties
	- Read/write coherence
	- Before-or-after atomicity
	- All-or-nothing atomicity

### Cell storage model

- Assumption on the storage
	- cells of the same size
	- each object fits exactly in one cell
	- Read/write unit is sector or block
- Reflects the physical organization of common storage media
	- primary memory - organized as an array of memory
	- secondary storage device (e.g, a disk) - organized in sectors or blocks
- Guarantee read/write coherence
	- not obvious

### Journal Storage model

- A model for storing complex objects like records consisting of multiple fields
- A manager + cell storage
	- The entire history of a variable (not only the current value) is maintained in the cell storage
	- No direct access to the cell storage; the user sends request to the journal manager
	- The journal manager translates user requests to commands sent to the cell storage
- The Log contain the history of all variable in the cell store
	- Each update of a data item is a record (containing all the necessary information) appended to the log
	- the log is authoritative
	- allows to reconstruct the cell store
- An all-or-nothing action (an online transaction)
	- first records the action in a log in journal storage
	- the installs the change in the cell storage by overwriting the previous version of a data item
- The log is always kept on nonvolatile storage
- The considerably larger cell storage resides typically on nonvolatile memory, but can be held in memory for real-time access or using a write-through cache

![journal image](https://ars.els-cdn.com/content/image/3-s2.0-B9780124046276000087-f08-02-9780124046276.jpg)

### Atomicity and storage models

- Cell storage
	- does not guarantee all-or-nothing atomicity
		- Once the content of a cell is changed by an action, there is no way to abort the action and restore the original content of the cell
	- Guarantee
		- read/write coherence
		- before-or-after atomicity
- Journal storage
	- guarantee all-or-nothing atomicity
		- When an action is performed first is modified the version history (log); then is modified the cell store (commit)
		- If one of the two operations fail the action is discarded (abort)

## Type of file systems

- Network file systems (NFSs)
	- Distributed; single point of failure
- Storage area networks (SANs)
	- flexible, resilient to changes in the storage configuration
	- Decouple compute nodes and storage nodes
	- Widely used in cloud computing
- Parallel file systems (PFSs)
	- Scalable, concurrent access, distribution of files across many nodes
	- SAN could be used as infrastructure 
	  
## High performance file systems

- Network file system
- Parallel file system
- Google file system
- Apache Hadoop

## Network File System

- Client-server model
- RPC interaction
- Does not scale

![NFS image](http://2.bp.blogspot.com/-bI4aQvXtsZA/TrL8SZNk_pI/AAAAAAAAB9c/BncGevPLHZU/s1600/nfs-implement.gif)
## General Parallel File System (GPFS)

- Allow multiple client to read and write concurrently from the same file
- Max file system size 4PB, 4096 disks of 1 TB each
- SAN can be used to implement the infrastructure
- Recovery from system failure based on write-ahead log file; updates are written to persistent storage only after the log records have been written
- Logs maintained separately by each I/O node for each file system mounted
- RAID to reduce the effect of node failures and stripping
- To improve fault tolerance data files and metadata are stored in 2 physical disks
- Locks are used to guarantee consistency
	- Central lock manager - local lock manager

![GPFS image](https://www.researchgate.net/profile/Marta-Mattoso/publication/278629912/figure/fig13/AS:282262206730251@1444307962936/GPFS-architecture.png)

## Google File System (GFS)

- It use thousand of storage system build from inexpensive commodity components to provide petabytes of storage
- Designed based on
	- high reliability to hardware failures, system software errors, application errors, human errors.
	- a careful analysis of the file charateristics
	- the access models
- Most important features of the "cloud" access model
	- files range in size froma a few GB to hundreds of TB
	- **append** rather than **random write**
	- **sequential read**
	- **response time** is not the main requirement; data are processed in bulk
	- relaxed consistency model to simplify the system implementation (consistency not in every point in time)
		- without placing an additional burden on the application developers

### Files in the GFS

- GFS file - collections of fixed-size segments called chunks 
	- chunk size 64MB; normal file systems work with blocks of 0.5-2 MB
	- Stored on Linux File System
	- Replicated on multiple sites (3 default - configurable)
- Large chunk size allows
	- to optimize performance for large files and to reduce the amount of metadata maintained by the system
	- to increase the likelihood that multiple operations will be directed to the same chunk
	- to reduces the number of requests to locate the chunk;
	- to maintain a persistent network connection with the server where the chunk is located
	- to reduce disk fragmentation; chunk for a small file and the last chunk of a large dile are only partially filled

### GFS Cluster Architecture

- A master
	- controls a large number of chunks servers
	- maintains metadata such as filenames, access control information, the location of all the replicas for every chunk of each file, and the state of individual chunk servers
- The location of the chunks
	- are stored only in the control structure of the **master's** memory
	- are updated at system startup or when a new chunk server joins the cluster
- This strategy allows the **master** to have up-to-date information about the location of the chunks

![GFS image](https://www.researchgate.net/publication/271891763/figure/fig1/AS:294992452046852@1447343089660/Google-File-System-Architecture.png)

### GFS File Access

- File access
	- Handled by the APP and Chunk server
	- The master grants a lease to a chunk server (primary)
	- The primary chunk server is responsible to handle the update
- File creation
	- Handled by the master

### GFS Write on a File

1. Client contacts the master which assigns a lease to one of the chunk servers (the primary) and reply with ID of primary and secondary chuck servers
2. Client send data to all chunk servers
	1. Chuck server store data in a LRU buffwe
	2. Chunk servers send ack to client
3. Client send write req to primary
	1. Primary apply mutation to file
4. The primary send write to secondary
5. Each secondary send ack to primary after mutation are applied
6. The primary ack the client

## Hadoop Distribued File System (HDFS)

- Apache Hadoop - software system to support processing of extremely large volumes of data (big data applications)
	- To implement dataflow/pipe&filter SW architectures
	- MapReduce + datastore (HDFS, Amazon S3, CloudStore, ...)
- HDFS is a distributed fs written in Java
	- it is portable, but it cannot be directly mounted on an existing operating system
	- not fully POSIX compliant, but it is higly performant
	- 64-128MB block size
- Replicates data on multiple nodes
	- three replicas default
	- large dataset distributed over many nodes

### HDFS Architecture

- Master/Slave Architecture
- NameNode (master)
	- manage the File System Namespace
	- store metadata
	- control access to files
	- record changes in a log
	- check DataNode liveness
	- Secondary Name node for high availability
- DataNode (slave)
	- Low level R/W ops

---

- [ ] https://www.edureka.co/blog/apache-hadoop-hdfs-architecture/



![Hadoop image](https://hadoop.apache.org/docs/r1.2.1/images/hdfsarchitecture.gif)
### HDFS cluster and block replica

![cluster image](https://techvidvan.com/tutorials/wp-content/uploads/sites/2/2020/03/hadoop-cluster.jpg)
![block image](https://miro.medium.com/max/602/0*WVTlR-s8gxuDDTEO)

### HDFS write protocol

- Three main stages
	- Set up of Pipeline
	- Data streaming and replication (Write pipeline stage)
	- Shutdown of Pipeline (Acknowledgement stage)

## NoSQL data store

- Motivations
- BigTable
- Dynamo

### Database

- Cloud applications
	- application => database => filesystem
	- low latency, scalability, high availability and consistency
- A **database management system (DBMS) should**
	- enforce data integrity
	- manage data access and currency control
	- support recovery after failure
Relational DBMS
- guarantees atomicity, consistency, isolation, durability (ACID)
- Usually not scalable (in speed and size)

*NoSQL* DBMS (or better data models)
- the structure of the data does not require a relational model
- the amount of data is very large (big data)
- may not guarantee the ACID properties
- key-value stores, document store databases, and graph databases

### Transaction processing - OLTP

- Many cloud applications
	- are based on **online transaction processing** (OLTP)
	- operate under tight latency costraints
- A major concer in OLTP systems is to reduce the response time
	- memcaching, is a solution - a general-purpose distributed memory system that caches objects in main memory (RAM)
- Scalability is the other major concern for cloud OLTP applications and implicity for datastores
	- vertical scaling - always possible
	- horizontal scaling - issue of consistency among multiple distributed copies / portion of the DB
- Relational databases not able to handle
	- the massive amount of data
	- the real-time demands
- Document stores and NoSQL databases
	- designed to scale well (horizontally)
	- do not exhibit a single point of failure
	- have built-in support for consensus-based decisions
		- Decisions taken on the majority of the votes
	- support partitioning and replication as basic primitives

## Transaction processing - NoSQL

- The "soft-state" approach in the design of **NoSQL** allows
	- data to be inconsistent
	- Implementation of ACID properties is on the application developer shoulders
- Data will be "eventually consistent" at some future point in time instead of enforcing consistency at the time when a transaction is "committed"
- Data partitioning among multiple storage servers and data replication
	- they increase availability, reduce response time, and enhance scalability

### BigTable

- BigTable is a distributed storage system developed by Google
- Achieve wide applicability, scalability, high performance and high availability
- Variety of workload
	- throughput-oriented batch-processing jobs
	- latency-sensitive serving of data to end users
- BigTable is based on a simple and flexible data model
	- Support dynamic control over data layout and format
	- Clients can control the locality properties of the data represented in the underlying storage by using appropriate schema
	- Data is indexed using row and column names that can be arbitrary strings
	- Data are treated as uninterpreted strings
	- The client can control serving data from memory or disk

### BigTable data model

- A BigTable is a sorted map
	- sparse, distributed, persistent, multidimensional
- The map is indexed by a row key a column key, and a timestamp
	- (row:string, column:string, time:int64) => string
- Each value in the map is an uninterpreted array of bytes (string)
- Example: Webtable

![big table image](https://images.slideplayer.com/32/10014970/slides/slide_2.jpg)

### BigTable Rows

- A row key is an arbitrary string of up 64 KB (10 - 100 byte is the typical size)
- R/W is atomic regardless the number of columns
- In a table, row are ordered lexicographically by row key
- A row range is dynamically partitioned into **tablets**, the units for load balancing
- Read of short row ranges are efficient and typically require communication with only a small number of machines
	- Clients can exploit this property to improve locality when accessing data
- Webtable Example:
	- The row key is the revers host name
	- maps.google.com/index => com.google.maps/index

### BigTable Columns

- Column key are grouped int sets called **column families**
- A column key is **family:qualifier** (Each column has a key)
- All the data stored in a column family are of the same type
- Column family's fata can be compressed
- A family must be created before storing data under any column key
- The number of family should be small up to hundres
- Families should rarely change
- Number of column in a family unbonded
- Number of column = number of families x number of keys per family

Example of families
- Language
- Anchor

Example of key
- **Language:ID**, content of the cell is IT, EN, SW, FR ...
- **Anchor:<link_of_referring_site>**, content the link text

Access control and disk/memory accounting is done at column family level

### BigTable Timestamps

- Each cell can contain multiple version of the same data
	- Versions are indexed by timestamp (64 bit integers)
	- Versions are stored in decreasing timestamp order
	- The most recent versions can be read first
- Timestamp assignment
	- by BigTable; represent real time in microseconds
	- by the client application; collisions must be avoided by generating unique timestamps
- Two mechanism to garbage collect cell version automatically
	- only the last n version of a cell to be kept
	- only new-enough versions be kept (e.g. latest 7 days)
- Example
	- Timestamp of contents column is "the times at which these page versions were actually crawled"
	- Only the 3 most recent versions are kept

### BigTable building blocks

- Uses Google File System
- Google **SSTable** file format
- Chubby, a highly-available and persistent distributed lock service

### Google SSTable

- Google **SSTable** file format is used internally to store Bigtable data
	- SSTable provides a persistent, ordered immutable map from keys to values
	- Each SSTable contains a **sequence of blocks** and a **block index** to locate blocks
	- To access an SSTable block
		- The index is loaded into memory when the SSTable is opened;
		- The block is found using binary search in memory
		- The block is access with a single seek on disk
	- An SSTable can be also loaded all in memory

### BigTable architecture

- Consist of one **master server** and many **tablet servers**
- A BigTable cluster store of a number of tables; each table consist of a set of tablets
- The **master** is responsible for:
	- assigning tablets to tablet servers
	- detecting the addition and expiration of tablet servers
	- balancing tablet-server load, and garbage collection of files in GFS
	- handling schema changes such as table and column family creations
- **Tablet servers** can be dynamically added/removed from a cluster to accomodate the workload
- Each tablet server
	- manages a set of tablets
	- handles read and write requests to the tablets that it has loaded
	- splits tablets that have grown too large (initially for each table there is only one tablet)

![BigTable image](https://image.slidesharecdn.com/nosqldatabases-slideshare-110227120448-phpapp01/95/nosql-databases-why-what-and-when-127-728.jpg?cb=1298888093)
### Amazon's Dynamo

- Highly available and scalable distributed key-value store built for Amazon's platform
	- many service on Amazon's platform that only need primary-key access to a data store
		- best seller lists, shopping carts, customer preferences, session management, sales rank, and product catalog
	- RDB would be inefficiencies and limit scale and availability
- Dynamo is used to manage the stat of service tha have very high reliability requirements
	- Reliability is one of the most important requirements
	- Outages have significant financial consequences
- Dynamo tradeoffs between availability, consistency, cost-effectiveness and performance
- Eventually consistent data store; that is all updates reach all replicas eventually
	- Optimistic replication techniques are used to increase availability in a environment prone to failures
	- In optimistic replication changes are allowed to propagate to replicas in the background
	- optimistic replication can lead to conflicting changes which must be detected and resolved

### Amazon's Dynamo requirements

- Query model
	- Simple read and write operations to a data item that is uniquely identified by a key ACID DB have poor availability (of data)
- Weak consistency to improve availability
	- Uses a consistency protocol similar to those used in quorum systems, i.e. if the majority of node complete successfully an operation the operation is successful
		- That increase availability
- Weak consistency to decrease latency
	- R min number of nodes that must participate in a successful read
	- W min number of nodes that must participate in a successful write
	- Setting R and W such that R + W > N the latency of a get (or put) operation is dictated by the slowest of the R (or W) replicas
	- If R and W are set to be less than N the latency decrease

### Key design principles - Data Replication

- Optimistic replication = Data replication + weak consistency
	- Allow propagation of changes asynchronously
	- Introduce the problem of inconsistency that is, detect and resolve conflict
- When to resolve conflict
	- Always writable
	- Conflict are resolved at read time
- Who resolve the conflict
	- The data store; limited
	- The application; more complex

### Other Key design principles

- Incremental scalability - be able to scale out one storage host at a time
- Symmetry - every node in Dynamo should have the same set of
responsibilities as its peers
- Decentralization - favor decentralized peer-to-peer techniques over
centralized control
- Heterogeneity - to exploit heterogeneity in the infrastructure

### System Architecture

- System interface
- Partitioning Algorithm
- Replication
- Data Versioning

### System Interface

- Get(key)
	- locates the object replicas associated with the key in the storage system
	- returns a single object or a list of objects with conflicting versions along with a context

- Put(key, context, object)
	- determines where the replicas of the object should be placed based on the associated key
	- writes the replicas to disk

- The context encodes system metadata about the object that is
opaque to the caller and includes information such as the version of
the object

### Partitioning Algorithm 

- To scale incrementally
	- Dynamic partitioning of data over a set of nodes
- Partitioning with consistent hashing to distribute the load across multiple storage hosts
	- Each node in the system is assigned a random value within this space which represents its “position” on the ring.
		- Each data item identified by a key is assigned to a node by hashing the data item’s key to yield its position on the ring
- Challenge 
	- Non uniform distribution and unbalance of the load
- The concept of virtual node is introduced
- If a node becomes unavailable
	- the load handled by this node is evenly dispersed across the remaining available nodes
- When a node becomes available again, or a new node is added to the system
	- the newly available node accepts a roughly equivalent amount of load from each of the other available nodes
- The number of virtual nodes that a node is responsible can be decided based on its capacity, accounting for heterogeneity in the physical infrastructure

![Dynamo image](http://paperplanes-assets.s3.amazonaws.com/consistent-hashing.png)

### Replication

- To achieve high availability and durability data is replicated on multiple hosts
	- Each data item is replicated at N hosts
- Each key, k, is assigned to a coordinator node
	- in charge of the replication of the data items that fall within its range 
	- store locally each key within its range
	- replicates these keys at the N-1 clockwise successor nodes in the ring

![Key replication image](https://i.stack.imgur.com/aW3vg.png)

### Data Versioning

- Eventual consistency allows for updates to be propagated to all replicas
asynchronously
- A put() call can return before all the replicas are updated
	- A subsequent get() may return an object that does not have the latest update
- Some application can tolerate such behavior, e.g. “add to chart”
	- An add can never be rejected
	- A change can be done on an older/not-updated version of the chart
	- Divergent versions will be reconciled
- The result of each modification is treated as a new and immutable version
	- Vector clock (node, counter) are used to mark the version and capture causality
	
![Versioning image](https://image.slidesharecdn.com/cap-131117230434-phpapp02/95/dynamo-and-bigtable-in-light-of-the-cap-theorem-26-638.jpg?cb=1384729712)


