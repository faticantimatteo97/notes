# NAT (Network Address Translation)

## Routed vs Transparent firewall modes
 
 - In routed mode, a firewall is a hop in the routing process
 	- it is a router responsible of its own (internal) networks
 - In transparent mode, a firewall works with data at Layer 2 and is not seen as a router hop to connected devices
 	- in can be implemented using bridged NIC

![firewall modes image](https://alexandremspmoraes.files.wordpress.com/2012/01/2012-01-19_blog_transparent-routed.jpg)

## Routable IP Addressing

All the routers in internet should be able to look at the ip address and take a decision on how to make that packet to reach the final destination.

- Routable addresses need to be unique on the Internet to be PUBLICLY reachable ⟶ **public IP addresses**
- Non-routable addresses ⟶ **Special-Purpose Address IP addresses
	- Private addresses (https://www.iana.org/go/rfc1918)
		- 10.0.0.0 - 10.255.255.255 (10/8 prefix)
		- 172.16.0.0 - 172.31.255.255 (172.16/12 prefix)
		- 192.168.0.0 - 192.168.255.255 (192.168/16 prefix)
	- Loopback addresses (https://www.iana.org/go/rfc6598)
		- 127.0.0.0–127.255.255.255 (127/8 prefix)
	- Shared address space (https://www.iana.org/go/rfc6598)
		- 100.64.0.0–100.127.255.255 (100.64/10 prefix)
	- Full list: [iana website](https://www.iana.org/assignments/iana-ipv4-special-registry/iana-ipv4-special-registry.xhtml)

## Network Address Translation (NAT)

Translate a network address so that we can actually make possible the connection from a private IP address to a public IP address
- Traslate the address (f.e between incompatible IP addressing)
- Informally speaking, connecting to the internet a LAN using un-routable in-house LAN addresses

NAT in a routed firewall:
- Can filter requests from hosts on WAN side to host on LAN side
- Allows host requests from the LAN side to reach the WAN side
- Does not expose LAN hosts to external port scans

### NAT goals

- A private network uses just one IP address provided by ISP to connect to the Internet 
- Private networks use private IP addresses provided by IETF
- Can change address of devices in private network without notifying outside world
- Can change ISP without changing the address of devices in private network
- Devices inside private network are not explicitly addressable by external network, or visible by outside world

### Source NAT (SNAT)

- Translate **outgoing** IP source packet headers from the internal host addresses to the WAN IP address
- The session is **masqueraded** as coming from the NATting device

Source NAT when the connection originate in the internal network and has to reach the outside

- The firewall/router:
	- Forward replies from the client service request by the external server to the client
	- Enables the client-server session or connection to continue on another port as requested by the external server,
forwarding any responses by the server to the client (the RELATED connections)
- The NAT table is where associations between requests and internal IP addresses are kept

![SNAT image](https://techdocs.f5.com/content/dam/f5/kb/global/solutions/K41572395_images.html/Outbound%20NAT.jpg)

## Types of NAT: Basic and NAPT

- Basic NAT: a block of external/public IP addresses are set aside for translating the addresses of hosts within a private domain as they originate sessions to the external domain
	- For packets outbound from the private network, the source IP address and related fields such as IP,TCP,UDP and ICMP header checksums are translated
	- For inbound packets the destination IP address and the checksums as listed above are translated

However, multiple external/public IP addresses are difficult to obtain due to shortage of IPv4 addresses
	- Network Address Port Translation (NAPT)
		- The NAPT also translates transport identifiers, e.g., TCP and UDP port numbers as well as ICMP query identifiers

### Network Address Port Translation

- Multiplex a number of private hosts into a single external/public IP address
- The IP address binding extends to transport level identifiers such as TCP/UDP ports
	- For most of the small office and home (SOHO) routers
	- The private network usually relies on a single IP address, supllied by the ISP to connect to the Internet
	- SOHO can change ISPs without changing private IP addresses of the devices within the network

![NAPT image](https://upload.wikimedia.org/wikipedia/commons/thumb/5/5f/NAPT-en.svg/420px-NAPT-en.svg.png)

### NAPT for Incoming Requests

- NAPT router blocks all incoming ports by default
- Many applications have had problems with NAPT in the past in their request handling of incoming requests
- Four major methods:
	- Application Level Gateways (ALG)
	- Static port forwarding
	- Universal Plug and Play (UPnP) Internet Gateway Device (IGD) protocol
	- Traversal Using Relays around NAT (TURN)

## Destination NAT (DNAT)

- Enables servers located **inside** the firewall/router LAN to be accessed by clients located outside.
- The service appears to be hosted by the firewall/router

- The firewall/router uses the NAT table to:
	- Translate incoming packets from the firewall/router WAN IP address to the internal address of the server
	- Forward the replies to the client service request from the internal server to the external client
	- Enables the client-server session or connection to continue on another port as requested by the internal server, forwarding
any responses by the client to the server (the RELATED connections)

- it is also called port forwarding or Virtual Server
- According to the port accessed from the external interface, the packets can be forwarded towards different internal hosts

## NAT PRO'S

- The need to establish state before anything gets through from outside to inside solves one set of problems
- The expiration of state to stop receiving any packets when finished with a flow solves a set of problems
- The appear to be attached at the edge of the network solves a set of problems
- The ability to have addresses that are not publicly routed solves yet another set of problems (mostly changes where the state is and scale requirements for the first one)

### Perceived benefits of NAT and impact on IPv4 (RFC 4864)

- Simple Gateway between Internet and Private Network
- Simple Security Due to Stateful Filter Implementation
- User/Application Tracking
- Privacy and Topology Hiding
- Independent Control of Addressing in a Private Network
- Global Address Pool Conservation
- Multihoming and Renumbering with NAT


