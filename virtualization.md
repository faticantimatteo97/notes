### Theme III - Virtualization

- [ ] Chapter 3 of the book
- [ ] Classroom materials
- [x] [what is a container](https://www.youtube.com/watch?v=EnJ7qX9fkcU)
- [ ] [bringing Filesystems together](https://www.linuxjournal.com/article/7714)
- [ ] [namespace](https://man7.org/linux/man-pages/man7/namespaces.7.html)
- [ ] [cgroups](https://man7.org/linux/man-pages/man7/cgroups.7.html)
	- [ ] https://facebookmicrosites.github.io/cgroup2/docs/overview
	- [ ] https://facebookmicrosites.github.io/cgroup2/docs/create-cgroups.html
	- [ ] https://www.kernel.org/doc/Documentation/cgroup-v2.txt
- [ ] [docker docs](https://docs.docker.com/)


**Why virtualization is used:**
:
- Increased performance and computing capacity
- Allow the use of underutilized hardware and software resources
	- With virtualization we can use the resources left unused from a set of application.
- Lack of space
	- Optimization of the resources
- Greening initiative
	- Use more power only when is necessary
- Rise of administrative costs
	- reduce the manual intervention needed

## Characteristics of virtualization environments

- Increased security
- Managed execution
- Portability

![virtualization image](https://slideplayer.com/slide/3406916/12/images/33/Guest+Virtualization+Layer+Software+Emulation+Host+Virtual+Hardware.jpg)


**Increased Security**

Every operation in the guest layer is monitored by the hypervisor, which can decide what is permitted or not.
In other word The virtual machine manager control and filter the activity of the guest, preventing some harmful operation from being performed.
- Resources exposed by the host can be hidden or simply protected from the guest
- Sensitive information that is contained in the host can be naturally hidden without the need to install complex security policies.

**Managed execution**

With virtualization we can manage the execution of application and virtual machine in four different ways.

- resource sharing
	- we have a server and on top of that we run many virtual machine
- aggregation
	- we have a pool of physical machine and we would like to make them appear as a single resource (typically the approach of cloud stored systems)
- emulation
	- happens when we have a particulare architecture and we want to run software developed for another architectures
- isolation
	- with managed execution we can isolate the different virtual machines

![execution image](https://ars.els-cdn.com/content/image/3-s2.0-B9780124114548000036-f03-02-9780124114548.jpg?_)

**Portability**

- A java or python code can be executed on any environment running a JVM or Python VM (python and java are not compiled languages)
- A VM is booted from a VM image that is stored in a specific disk format
- A Docker container can run on any platform running the Docker engine


## Virtualization techniques

**Operating system level virtualization**

We dont need machine manager,hypervisor or hardware emulation sharing the same host OS. We only need a server with an operative sistem installed and we can run containers on it.

Based on tree main technology:
- Namespace (what you can see and use)
- Cgroups (how much resources the container can use)
- unionFS (file system that allow to create and manage the container images)

Containers are an evolution of the **chroot** mechanism
- the **chroot** operation changes the file system root directory for a process and its children to a specific directory
- the process and its children cannot have access to other portions of the file system than those accessible under the new root directory
- unix systems also expose devices as part of the file system, by using this method it is possible to completely isolate a set of process

![container image](https://cloudblogs.microsoft.com/uploads/prod/sites/37/2019/07/Demystifying-containers_image1.png)

in containers the isolation is weaker than virtual machine because the OS is shared. To strengthen security we need to filter each call of the container at the operating system level.

### Process virtualization with Docker

- Containers run directly within the host machine's kernel, 2 different types:
	- Containers + bare metal
	- Containers + virtual machine

- Docker container manager is an application
	- CLI command line interface to interact with the server (Docker Daemon)
	- CLI use the DOcker REST API
	- Docker REST API can be used also separately (called from applications)
	- The Daemon create and manage Docker objects
![docker image](https://www.guruadvisor.net/images/numero17/docker/docker-engine.png)


### Docker architecture

A container is like a box in wich we put everything needed to run the application.

![docker architecture image](https://geekflare.com/wp-content/uploads/2019/09/docker-architecture-609x270.png)


### Example of a docker CLI command

```bash
docker run -i -t ubuntu /bin/bash
```

The Docker daemon does the following actions:

1. Locate and eventually download the ubuntu image
2. Create a new container
3. Allocate a R/W file system as final layer of the image
	- isolated from the host file system
4. Create a network interface connected to the default network
5. Start the container and run /bin/bash command

## Underlining technologies

- Namespace to create isolation
	- When docker run a container a set of namespaces is created for the container and the container can access only that namespaces
- Control groups cgroups	
	- Limit and constraints on the use of resources (CPU,Memory)
- Union file system unionFS
	- File systems that operate creating layers
- Container format
	- A wrap of namespaces, cgroups and unionFS
	- libcontainer is the default format

### Namespace

A namespace wraps a global system resource in an abstraction that makes it appear to the processes within the namespace that they have their own isolated instance of the global resource.
Changes to the global resource are visible to other processes that are members of the namespace, but are invisible to other processes.
One use of namespaces is to implement containers.

- **Cgroup** Cgroup root directory
- **IPC** System V IPC, POSIX message queues
- **Network** Network devices, staks, ports, etc.
- **Mount** Mount points
- **PID** Process IDs
- **User** User and group IDs
- **UTS** Hostname and NIS domain name

### Cgroup

Control groups, usally referred to as cgroups, are Linux kernel feature which allow processes to be organized into hierarchical 
groups whose usage of varius types of resources can be limited and monitored.

- The kernel's cgroup interface is provided through a pseudo-filesistem called cgroupfs.

With cgroup we can control Memory, CPU, process ID, input/output resources, direct memory access.

![cgroup core image](https://facebookmicrosites.github.io/cgroup2/docs/assets/HighLevelArch.png)

![cgroup controller image](https://facebookmicrosites.github.io/cgroup2/docs/assets/hierarchyEXTERNAL.png)

### UnionFS

- Unioning allows administrator to keep files separate physically, but to merge them locally into a single view
- Union: a collection of merged directories; each physical directory is called a branch
- UnionFS simultaneously layers on top of several filesistems or directories
	- presents a filesystem interface to the kernel
	- it can be employed by any user-level application or from the kernel by the NFS server
	- intercepts operation boud for lower-level filesystems, it can modify operations to present the unified view

The directory on the Union File System are called branch.

- To each branch is assigned a precedence priority
- A branch with a higher precedence overrides a branch with a lower precedence
- Unionfs operates on directories
	- if a directory exists in two underlying branches, the contents and attributes of the Unionfs directory are the combination of the two lower directories
- Unionfs automatically removes any duplicate directort entries
	- if a files exist in two branches, the contents and attributes of the Unionfs file are the same as the file in the higher-priority branch, and the file in the lower-priority branch is ignored

### Copy-on-Write Unions

- Unionfs can also mix read-only and read-write branches
- In this case, **the union as a whole is read-write**
- copy-on-write semantics **to give the illusion that you can modify files and directories on read-only branches**

### Layers concept

We can specify all the layers we need in a container using a **Dockerfile**
1. first we specify what is the image where we would start build a container
2. then we run a COPY command where we copy the application on top of the OS layer
3. we build and run our application using the RUN command
4. the CMD run the application

![layer image](https://docs.docker.com/storage/storagedriver/images/container-layers.jpg)

**The R/W layer is not persistent.**

### Storage

Different options:
- Volumes
	- a way to store the data and make isolated from the host
	- created and managed by docker (isolated from the host)
	- mounted  into multiple containers simultaneously
- Bind mount
	- share the data with the host filesistem
	- any area of the host file system
	- eventually created by the container
	- shared with host processes
	- not portable
	- performance usually higher than volumes but depends on the host system and storage
- tmpfs mount
	- not persistent, stored in memory
	- a memory area in the container namespace or shared with the host

### Docker services

With docker service we can aggregate multiple containers and scale them in a Swarm.

- A Swarm is a set of docker deamons (managers) and containers (workers)
	- In the simplest deployment: 1 manager, 1 or more workers
- A Docker service allows to define
	- How many container should be avalaible
	- Constraints on the use of resources
	- Load balancing
	- Recovery procedures

The description of a service is specified on the docker-compose.yml file.

### Networking

- You can connect Container and Service together, or connect them to non-Docker workloads
	- The networking is always transparent

### Network drivers

- **bridge**
	- The default network driver
	- Bridge network are usually used when your applications run in standalone containers that need to communicate
	- You don't share the network of the host
	- Better when you need multiple containers to communicate on the same DOcker host
- **host**
	- For standalone containers
	- Remove network isolation between the container and the Docker host, and use the host's networking directly
	- Host is only available for swarm services on Docker 17.06 or higher
	- Better when the network stack should not be isolated from the docker host, but you want other aspects of the container to be isolate
- **overlay**
	- Overlay network connect multiple Docker daemons together and enable swarm services to communicate with each other
	- You can also use overlay network to facilitate communication between a swarm service and a standalone container,or between two standalone containers on different Docker daemons
	- This strategy removes the need to do OS-level routing beetween these container
	- Better when you need containers running on different Docker host to communicate, or when multiple applications work together using swarm services
- **macvlan**
	- Allow you to assign a MAC address to a container, making it appear as a physical device on your network
	- Better when migrating froma a VM setup or you need your container to look like physical hosts on your network, each with a unique MAC address
	- The Docker daemon routes traffic to containers by their MAC addresses
- **none**
	- disable all networking


## Virtualization tecniques

### Machine reference model

![machine reference image](https://ars.els-cdn.com/content/image/3-s2.0-B9780124114548000036-f03-04-9780124114548.jpg)

### Privileged and NonPrivileged instructions

- **Nonprivileged:** instructions can be used without interfering with other tasks
	- floating, fixed-point, and arithmetic instructions
- **Privileged:** instructions are executed under specific restrictions and are mostly used for sensitive operations
	- **behavior-sensitive** instructions operate on the I/O
	- **control sensitive** instructions alter the state od the CPU registers

### Hypervisor

- literally means, run on top of the supervisor mode
- in practice, hypervisor run in supervisor mode

**Challenge:** to fully emulate and manage the status of the CPU for guest
operating systems all the sensitive instructions will be executed in privileged
mode, which requires supervisor mode

![hypervisor image](https://img.vembu.com/wp-content/uploads/2019/12/Hypervisor-Types.png)

Two types of hypervisor:
- **Type I:** the virtual machine manager embedd the operating system, a layer less in respect to type II
- **Type II:** run on top of the operating system, and then the operative system expose the application binary interface to the hypervisor, which will expose the instruction set architecture to the virual machines

![hardware virtualization image](https://image.slidesharecdn.com/virtualizationfinal-140301120900-phpapp02/95/introduction-to-virtualization-27-638.jpg)

### Hypervisor key components

- **Dispatcher:** receive the call for an instruction set and decide if send them to the allocator or intepreter
- **Allocator:** responsible for deciding the system resources to be provided to the VM
- **Interpreter:** if the call is for a privileged instruction it goes to the interpreter routines
	- check if the execution will not compromize the virtual machine and then will execute the instruction

### VMM: Formal requirements

- **Equivalence:** A guest running under the control of a virtual machine manager should exhibit the same behavior as when it is
executed directly on the physical host
- **Resource control:** The virtual machine manager should be in complete control of virtualized resources
- **Efficency:** A statistically dominant fraction of the machine instructions should be executed without intervention from the
virtual machine manager

## Full virtualization

- The virtual machine manager scan the instruction stream
	- Noncritical instructions run on the hardware directly
	- Privileged, control and behavior sensitive instructions are identified and trapped into the VMM, which emulates the behavior of these instructions
	- **Binary translation** is the emulation technique used

- The guest OS is completely decoupled from the underlying hardware
	- unaware that is being virtualized

- Binary translation id very costly in terms of performance especially for I/O operations

### Binary Translation

Sequence of instructions are translated from a source instruction set to the target instruction set

- **Static:** aims to convert all the code of an executable file without having to run the code first
- **Dynamic:** looks at short sequence of code then traslates it and chaches the resulting sequence
	- Code is only traslated as it is discovered and when possible, branch instructions are made to point to already translated and saved code
	- More overhead than static translation because the cache have limited size.

## Hardware assisted virtualization

Reduce the performance penalities experienced by emulating x86 hardware with VMM- By design the x86 architecture did not meet the Equivalence requirements

The instructions set is extended to include operation to directly manage virtual machines or implement the most frequents instruction used when we run VMs on hardware.

**VMware** is an example.

## Paravirtualization

- Not-trasparent
	- A para-virtualized VM provides special APIs requiring substantial OS modifications in user applications
- Para-virtualized OS are ssisted by an intelliged compiler to replace nonvirtualizable OS instructions by hypercalls
- Pro
	- preventing performance losses
- Cons
	- guest operating systems need to be modified
	- compatibility and portability may be in soubt
	- high cost of maintaning para-virtualized OSes

KVM and Xen are examples.

### Xen

Is an example of paravirtualization in which domains represent VM istances and domain 

- Domain 0 host specific control software which has privileged access to the host and controls all the other guest operating systems
	- first one that is loaded once the virtual machine manager has completely booted
	- it hosts a HTTP server that serves requests for virtual machine creation, configuration and termination


## VM migration

- Off-line migration, the VM is stopped, moved and restarted
- Live migration, the VM is transferedd without generating service discontinuity

![VM migration image](https://www.researchgate.net/profile/Md-Hasanul-Ferdaus/publication/276849142/figure/fig3/AS:614295171784717@1523470790685/Stages-of-the-pre-copy-VM-live-migration-technique.png)


