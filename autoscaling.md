## Autoscaling

## Materials

- [ ] [Microservices](https://martinfowler.com/articles/microservices.html)
- [ ] Containers autoscaling
	- [ ] survey (2018) book chapter in Systems modeling methodologies ad !tools
	- [ ] https://kubernetes.io/docs/tasks/run-application/horizontal-pod-autoscale/
	- [ ] https://github.com/kubernetes/community/blob/master/contributors/design-proposals/autoscaling/horizontal-pod-autoscaler.md

## Scalability

- Performance measure how fast and efficiently a SW system can complete certain tasks
- Scalability measure the trend of performance with increasing load
	- A system is scalable if it is capable to mantain performance under increased load by adding more resources

Two tipe of scalability:
- linear scalability: if you increase the load is enough to add more resources to proportionally increase the performance
- non-linear scalability: the performance don't scale well with the number of resources added

### What we need to scale a system?

- Resource usage monitor
- Scaling mechanism
- Load balancer

## Autoscaling mechanism

- Course grain - Replication of an application
	- Typically imply the deployment of a new VM
	- With container technology could be also the replication of a container or a set of containers inside a VM
- Fine grain - Replication of an application components (micro-services)
	- Micro-services is a SW architecture that decompose an application in independet components
	- Running a micro-service in a container makes it possible to replicate the hotspot components of an application

The microservice architectural style is an approach to developing a
single application as a suite of small services, each running in its own
process and communicating with lightweight mechanisms, often an
HTTP resource API. These services are built around business
capabilities and independently deployable by fully automated
deployment machinery.


### Automated Scaling listener

- Core component of the Dynamic Scaling Architecture
	- Automatic scaling
	- Automatic notification

Not pratical in most scenarios.

![asl image](https://patterns.arcitura.com/wp-content/uploads/2018/08/fig2-80.png)


## Dinamic Scalability Architecture

- Predefined algorithms define the scaling conditions that trigger the
dynamic allocation of IT resources from resource pools
- The automated scaling listener could take decisions on the basis of:
	- workload counters (e.g. number of service requests)
	- Performance counters/metrics (e.g. CPU utilization, throughput, response time)
	- ... and a combination of that and more complex counters/metrics
- The autoscaling algorithm determines how many additional IT resources can be dynamically provided
- Scaling actions:
	- Scale-out/in (horizontal scaling)
	- Scale-up/down (vertical scaling)
	- Migration / relocation to/on a system with more/less capacity

**Workflow:**

1. Cloud service consumers requests
2. The automated scaling listener (ASL) monitors the cloud service to determine if predefined capacity thresholds are being exceeded
3. Consumer requests increases, threshold are violated
4. A scaling policy is used to determine the needed scaling actions
5. The ASL initiates the scaling process, if needed
6. The ASL sends a signal to the resource replication mechanism (RR)
7. RR creates more instances of the cloud service 
8. Now that the increased workload has been accommodated and the process start again from (2) to follow the workload


## Autoscaling algorithms

- Threshold based - reactive
	- Thresholds on workload or resource utilization
	- When a threshold is exceed the scaling action is triggered
	- Multiple thresholds can be set to introduce a “sort of” proactivity
	- Is heuristic and not optimal (over/under provisioning)
- Model based - reactive
	- Is defined a mathematical model of the system; the solution of the model give the scaling action to be performed (scale out/in or up/down)
	- Triggered by events set on performance metrics values and workload intensity
	- Could be optimal
- Proactive (threshold / model based)
	- The values of the workload, resource utilization, performance metrics are predicted in the short term (e.g. 5 - 10 minutes ahead)
	- The algorithm (the same as above) take decisions on the predicted values
	- Could be mixed with reactive to adjust the adaption on the basis of the actual workload

### Autoscaling in AWS

- Concepts
	- Autoscaling group – a collection of EC2 instances that share similar characteristics
	and are treated as a logical grouping for the purposes of instance scaling and management
	- Launch configuration – a template that an Auto Scaling group uses to launch EC2 instances
	- Scaling plan (or scaling policy) – the policy that determine the scaling actions
		- Maintain current instance levels at all times
		- Manual scaling
		- Scale based on a schedule (scheduled scaling)
		- Scale based on demand (dynamic scaling)
- Launch configuration and Scaling plan are associate to a scaling group
- A scaling group can be configured with multiple scaling policies
	- At least one for scaling out and one for scaling in

### Autoscaling life cycle

![autoscaling image](https://docs.aws.amazon.com/it_it/autoscaling/ec2/userguide/images/auto_scaling_lifecycle.png)

### Dynamical scaling in AWS

- Simple scaling and Step scaling
	- You define how many istance to add/remove and when
	- When is given by a threshold on performance metric values
- Target tracking scaling
	- You define a desired value for a performance or workload metric, e.g. CPU utilization, request count per target
	- Tip: use high monitoring frequency to be more responsive

``` 
do {
   Listen for the occurrence of an Alarm (event associated with aggregated metric value);
   If (alarm occur) then
      pass the control to the component that execute the scaling action associated to the alarm
   end_if
} while true; 
```

AWS Scaling adjustment types (Scaling policies)

- Change in capacity (capacity = number of VMs)
	- You specify the adjustment in capacity(+/-)
	- If the current capacity of the group is 3 istances and the adjustment is 5, then when the policy is performed, Auto Scaling adds 5 istances to the group for a total of 8 istances
- Exact capacity
	- You specify the new capacity of the scaling group
	- If the current capacity of the group is 3 istances and the adjustment is 5, then when this policy is performed, Auto Scaling changes the capacity to 5 istances
- Percent capacity
	- You specify the percentage of increment/decrement
	- If the current capacity is 10 instances and the adjustment is
10 percent, then when this policy is performed, Auto Scaling
adds 1 instance to the group for a total of 11 instances
	- Rounding rules are defined, example:
		- Values greater than 1 are rounded down
		- Values between 0 and 1 are rounded to 1
		- ...

In all the scaling policies must be defined the maximum and minimum scaling group size
- A scaling adjustment cannot change the capacity of the group above the maximum group size or below the minimum group size

### Aggregated metrics

- At a given instant of time *t* the "Performance Metric value" used by the scaling policy to take a scaling decision is the aggregated of the "Performance Metric value" at time *t* collected from all the instances for that metric

### Warm-up

- Number of seconds that it takes for a newly launched instance to work at its full capacity is called **warm-up**
- Until its specified warm-up time has expired, an instance is not counted toward the aggregated metrics of the Auto Scaling group
	- Not doing that will result in an unrealistic metric

### Alarms

- A scale-in activity can't start while a scale-out activity is in progress
- Alarms
	- The alarm event occur when a metric value is upper/lowe a desired threshold for a specific time period

### Step Scaling mechanism (wrap-up)

1. ASL listens for the occurrence of an Alarm
2. If alarm X occur, pass the control to the component that
	- execute scaling for X (change in capacity, exact capacity, percent capacity)
		- Add/remove EC2 instance, and/or Launch an EC2 instance to replace a failed one
	- Wait warm-up period before counting the new instances toward the aggregated metrics of the Auto Scaling group
3. Return listening (Step 1) without waiting the new instances are up and running and the worm up period expired

## Autoscaling in containers

- Swarm allows to mantain a certain capacity (number of container)
	- Other components are needed to enable autoscaling
		- Orchestration and monitoring, e.g. Kubernetes + cAdvisor
- Orchestration
	- Container orchestration allows cloud and application providers to define how to select, to deploy, to monitor, and to dynamically control the configuration of multicontainer packaged application in the cloud
- Kubernetes
	- https://kubernetes.io/

## Kubenetes: main features

- Service discovery and load balancing
	- To expose a container using the DNS name or using their own IP address	- To load balance and distribute the network traffic so that the deployment is stable
- Storage orchestration
	- To automatically mount a storage system of your choice, such as local storages, public cloud providers, and more.
- Automated rollouts and rollbacks
	- To describe the desired state for deployed containers
		- it can change the actual state to the desired state at a controlled rate
	- For example, you can automate Kubernetes to create new containers for your deployment, remove existing containers and adopt all thei resources to the new container.
- Automatic bin packing
	- Management of a cluster of nodes to run containerized tasks
	- Description of how much CPU and memory (RAM) each container needs
	- Fitting of containers onto cluster nodes to make the best use of resources
- Self healing
	- restarts containers that fail, replaces containers, kills containers that don't respond to user-defined health check
	- doesn't advertise failed containers to clients until they are ready to serve
- Secret and configuration management
	- Storage and management of sensitive information, such as passwords, OAuth tokens, and ssh keys.
	- Deployment and update of secrets and application configuration without rebuilding container images, and without exposing secrets in stack configuration.

### Kubernetes components

https://kubernetes.io/docs/concepts/overview/components/

![Kubernetes image](https://miro.medium.com/max/981/1*HXbT0c4Q5XaiCIp6y3VMvw.png)

### Horizontal Pod Autoscaling algorithm

- Based on CPU utilization of the pods
- CPU utilization is the recent CPU usage of a pod (average across the last 1 minute) divided by the CPU requested by the pod

- [ ] https://github.com/kubernetes/community/blob/master/contributors/design-proposals/autoscaling/horizontal-pod-autoscaler.md

## Load Balancing

### Load Balancer (for application workload)

- A device or a software that balance a workload across two or more resources
- Content-blind distribution
	- request are distributed in a pseudo-uniform way (e.g round-robin)
- Asymmetric Distribution
	- larger workloads are issued to IT resources with higher processing capacities or more avalaible capacity e.g. weighted round robin
- Workload Prioritization
	- workloads are scheduled, queued, discarded, and distributed according to their priority levels
- Content-Aware Distribution
	- requests are distributed to different IR resources as dictated by the request content

![Load balancing image](https://miro.medium.com/max/1586/1*tEaZGz-p1-E2ytNjl5RPJg.jpeg)


- 2 types of load balancer
	- Layer-4
	- Layer 7

- One way or Two way load balancing

![layer image](https://www.blog.anupchhetri.com/wp-content/uploads/2019/01/layer_4_7_load_balancing.png)

### Service load balancing

- service load balancing is related to how to balance and distribute the workload among many istances of a distributed application
- AWS offers
	- Elastic Load Balancer
		- Classic load balancer (layer-4 and layer-7) - replaced by the following
		- Network load balancer (layer-4)
		- Application load balancer (layer-7)
	- The ElasticBeanstalk
		- automatically take care of deploying an application on a set of EC2 istances, balancing the load and scaling the number of your application istances

### Elastic Load Balancer (AWS) - network

- Listener (& rule)
	- protocol:port
- Target
	- A resource - istance ID or IP address
	- can be regustered with multiple target groups
- Target group
	- A set of targets
	- Associated to a listener
- Health check
	- Of the load balancer
	- Of the istances

### Elastic Load Balancer (AWS) - application

- Listener configuratioj
	- protocol:port
- Listener rules
	- priority, action, optional host condition, and optional path condition
	- Rules are evaluated in priority order, from the lowest value to the highest value
- Targer
	- A resource - istance ID or IP address
	- can be registered with multiple target groups
	- Support sticky sessions (with coockis)
- Target group
	- A set of targets
	- Associated to a listener
- Health check
	- Of the load balancer
	- Of the istances

### Listener rules

- Host conditions
	- the host name in the host header
		- Uniroma1.it, elearning.uniroma1.it, *.uniroma1.it
- Path conditions
	- Specify the path in the domain
		- 128 characters: 128 characters: A–Z, a–z, 0–9, _ - . $ / ~ " ' @ : +, &
		- 2 wildcard: * (match any char), ? (match exactly 1 char)
	- /img/*
	- /img/

### Health check (application and network ELB)

Based on ping

- Response Timeout (RT)
	- The amount of time to wait when receiving a
	response from the health check, in seconds.
	- Valid values: 2 to 60; Default: 5
- HealthCheck Interval (HCI)
	- The amount of time between health checks of
	an individual instance, in seconds.
	- Valid values: 5 to 300; Default: 30
- Unhealthy Threshold (UT)
	- The number of consecutive failed health checks that must occur before declaring an EC2 instance unhealthy.
	- Valid values: 2 to 10; Default: 2
- Healthy Threshold (HT)
	- The number of consecutive successful health
checks that must occur before declaring an EC2
instance healthy.
	- Valid values: 2 to 10; Default: 10

## Monitoring

### Monitoring agents

- Is an intermediary, event-driven SW component that
	- exists as a service agent
	- resides along existing communication paths
	- trasoarently monitor and analyze dataflows
- Commonly used to measure network traffic and message metrics
- The monitoring agent intercepts the message to collect relevant traffic data (2) before allowing it to continue to the cloud service (3a)
- Is a one-way mechanism

### Resource agents / push model

- Is a processing module that collect usage data
	- Based on an event-driven interactions model
- Used to monitor usage metrics
	- Metrics are sent by the monitored resource to the resource agent
	- The resource agent store the values in the log databse when an event occurs
		- e.g. time period expire, initiating, suspending, resuming, and vertical scaling

### Polling agents

- Is a process module that collects cloud service (e.g. a VM) usage data by polling resources
- Used to periodically monitor resource status
	- Resource usage, traffic, uptime and downtime
- Example
	- The polling agent sending periodic polling request messages and receiving polling response messages
	- when the value in the answe message change, that is the state of the monitored server change, the value is recorded

### Monitoring in AWS: CloudWatch

- Based on a push model (In theory)
	- Implemented with a publish/subsribe mechanism
- CLoudWatch subscribes to a specific data source (generated by a resource)
- The resource publishes data

### CloudWatch main concepts

- Namespace - a container for metrics
	- e.g. EC2, EBS
- Metric - a time oriented set of datapoints published to CloudWatch
	- Uniquely defined in a namespace
	- Metrics expires after a period between 3h - 15 months (can't be deleted manually)
	- Each data point is marked with a **time stamp**
- Statistics
	- Min, max, average, sum, number of data points, percentile
	- Computed over a period of time: 1,5,10, 30, 60 second or multiple of
60sec (The offer could change)
	- E.g. if the metric is CPU utilization and you specify to collect the average statistic over a period of 30 sec cloud watch compute the average value
of the metric over the received data point in the last 30 secs.
- Alarms
	- Used to automatically initiate actions on your behalf
        - Defined for a single metric over a specified time period
        - Can perform one or more specified actions, based on the value of the
metric relative to a threshold over time
		- The action is a notification sent to an Amazon SNS topic or an Auto Scaling policy

## How to test scaling-in / out

Generate a workload for the system, continuously increase/decrease the load
- The increase of the load should be continuos
	- There could be benchmarks that allow to automatically increase the load upon a certain amount
	- Other benchmark require you run it many times with increased load values
		- do not let cooldown the system between two runs

### How to select appropriate scaling metrics

- It depends on the workload
- The workload typically is
	- CPU bound
	- Network bound
	- Disk bound
- CPU bound ⟶ CPU utilization
- Memory bound ⟶ Memory utilization
- Network bound ⟶ Number of connections, incoming traffic
- Disk bound ⟶ number of R/W transaction per second, number of byte R/W per second

### How to select appropriate scaling metrics: examples

- Web site
	- Number of incoming connection
	- Incoming traffic (byte/sec)
- Application that does number crunching or other CPU intensive computations
	- CPU utilization
- In memory database (Cassandra/mongoDB)
	- Memory utilization
	- CPU utilization
- Application that read/write large files from disk or does an high number of read/write from disk
	- number of R/W transaction per second, number of byte R/W per second

### How to set appropriate threshold values

- Is typically an heuristic process
	- The rule of thumb - 66% of maximum capacity
	- Results of simulation studies
	- Adaptative thresholds
- In our case
	- For CPU utilization
		- 66%-70% is the safe level of utilization for a systek
		- Hence you can setup threshold around this value
	- For Network traffic (https://cloudonaut.io/ec2-network-performance-cheat-sheet/)

### How to set appropriate threshold values to show that your system scale in/out

- If under the load that you generate, your application demand for a maximum CPU utilization = 50% you can setup a threshold at 35%
- If your application generate maximum 3000 req/sec you can setup a threshold at 2000 req/sec
- ... and so on
	- always applying the 66% rule on the maximum value

### How to test availability

- Generate random failures by
	- killing services or
	- stopping VMs/container
- Manually or automatically
