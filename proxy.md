# Proxies

- Forward proxy
- Reverse proxy
- Application proxy
- Transparent proxy

### Proxy history: forward proxy

- A WWW proxy server provides access to the Web for people on closed subnets who can only access the internet through a firewall machine
	- Ari Luotonen, CERN Kevin Altis, Intel, April 1994
- Original idea: *An application-level proxy makes a firewall safely permeable for users in an organization, without creating a potential security hole through which "bad guys" can get into the organizations net.*
	- Namely: one single host handling requests from several users.

### Normal HTTP transaction

![http image](http://semi-retired.com/kevin_altis/papers/Proxies/Fig2.gif)

### Proxied HTTP transaction

![proxied http image](https://www.w3.org/History/1994/WWW/Proxies/Fig3.gif)

the host of the http request is the proxy itself, the it will make a new request asking the true resources.
	- we must tell the proxy the final host

- Proxy used for filtering (before reaching the firewall)

### Proxied FTP transaction

![proxied ftp image](https://www.w3.org/History/1994/WWW/Proxies/Fig4.gif)

Using a proxy to make a different request with a different protocol

### Other benefits of forward proxy

- Authentication, Authorization, Auditing, whitelisting, blacklisting
- Caching
	- store the retrieved document into a local file for further use so it won't be necessary to connect to the remote server the next time that document is requested
	- Problems:
		- How long is it possible to keep a document in the cache and still be sure that is up-to-date?
		- How to decide which documents are worth caching and for how long?
	- Solutions:
		- HEAD http request (very inefficient) [Can user the header information to see if something has changed]
		- ``` If-Modified-Since ``` request header (similary to modern web browsers)

## Forward proxy

- HTTP requests
	- Standard request in absolute-form to the proxy
	- The proxy will be in the middle-poin, forwarding the request towards the final termination
- Other (non-HTTP requests)
	- HTTP tunneling
	- HTTP CONNECT request with absolute-form to the proxy
		- The proxy establishes the TCP connection and become the middle-point

### HTTP tunneling: HTTP CONNECT

- Allow the use of any protocol that uses TCP
	- [ ] https://tools.ietf.org/html/draft-luotonen-web-proxy-tunneling-0
- Idea: the proxy simply receives the destination host the client wants to connect to and establishes the connection on its behalf
- Then, when the connection is established, the proxy server continues to proxy the TCP stream **unmodified** to and from the client
- Clearly, the proxy can perform authentication, whitelisting, and so on before accepting to forward the stream of data

(it is like a byte to byte relay, proxy is forwarding a stream of bytes)

### HTTP CONNECT method

![http connect image](https://www.igvita.com/posts/11/xconnect-proxy.png.pagespeed.ic.BFGcEIx6GY.png)

- [ ] https://tools.ietf.org/html/rfc7231#section-4.3.6
- Anything that uses a two-way TCP connection can be passed through a CONNECT tunnel
	- Example: HTTP forwardin SSL/TLS
- Not all proxy servers support the CONNECT method or limit it to port 443 only

## Content-filtering proxy

- After user authentication, HTTP proxy controls over the content that may be relayed
	- In schools, no Facebook or porn websites
		- Blacklists or semantic searches
	- Virus, malware scan
	- No files with executable or watermarking and so on

## Anonymizer proxy

- A proxy server that acts an intermediary and privacy shield between a client computer and the rest of the internet
- It accesses the internet on the user's behalf, protecting personal information by hiding the client computer identifying information (IP address, firstly)
- The server sees requests coming from the proxy address rather than the actual client IP address
	- Typical use for accessing restricted content (like The pirate bay and similar)
		- https://spys.one/en/
		- https://free-proxy-list.net/

## SSL Forward proxy

- A way to decrypt and ispect SSL/TLS traffic from internal users to the web, generally implemented in firewalls
- SSL Forward Proxy decryption prevents malware concealed as SSL encrypted traffic from being introduced in the network
- How it works:
	- The proxy uses certificates to establish itself as a trusted third party to the session betweeb the client and the server
	- As the proxy continues to receive SSL traffic from the server that is destined for the client, it decrypts the SSL traffic into clear text traffic and applies decryption and security profiles to the traffic
	- The proxy, then re-encrypts and forwards the traffic to the client

## Reverse proxy

- Forward proxy operates on behalf of the client
- Reverse proxy operates on behalf of the server
- It receives the requests from the outside as if it were the server and then forwards the request to the actual destination (origin) server
- Typical functions:
	- Load balancing
	- Cache static content
	- Compression
	- Accessing several servers into the same URL space
	- Securing of the internal servers
	- Appliceation level controls
	- TSL acceleration

![Forward proxy image](https://kinsta.com/it/wp-content/uploads/sites/2/2020/08/inoltrare-server-proxy-vs-proxy-inverso-server.png)

### Internal server protection

- The reverse proxy receives the requests from the clients and then issues new, prim and proper requests to the real server
- No direct connection with the outside also means defense against DoS
- Can provide support for HTTPS to servers that only have HTTP
- Can add AAA to services that do not have them
	- Example: a Iot device behind a firewall that must be accessible from the Internet

### Reverse proxy for Iot access

![iot proxy image](https://tinkerman.cat/post/secure-remote-access-to-your-iot-devices/images/Nginx-SSL-Reverse-Proxy.jpg)

- [ ] https://tinkerman.cat/post/secure-remote-access-to-your-iot-devices

Can make iot devices accessible from the outside without esposing them to the internet (adding client authentication for securing the access)

### Reverse proxy for application control: application firewall

- Application layer firewall operates at the application layer of a protocol stack
	- A WAF (*Web Application Firewall*) inspects the HTTP traffic and
	prevents attacks, such as SQL injection, cross-site scripting (XSS),
	file inclusion, and other types of security issues
		- Example: ModSecurity for apache webserver
- It can block application input/output from detected intrusions or malformed communication, or block contents
that violate policies
- It can detect whether an unwanted protocol is being provided through on a non-standard port or whether a
protocol is being abused in any harmful way

### TLS acceleration

- The SSL/TLS "handshake" process uses digital certificates based on asymmetric or public key encryption technology
- Public key encryption is very secure, but also very processor-intensive and thus has a significant negative impact on performance
	- SSL bottlenecks
- Possible solutions:
	- SSL acceleration: use hardware support to perform modular operations with large operands
	- SSL offloading: use a dedicated server only for SSL handshake

### SSL offloading

- SSL Termination
	- The proxy decrypts the TLS/SSL-encrypted data and then sends it on to the server in an unencrypted state
	- This also allows IDS or application firewall inspection
- SSL Forwarding (or Bridging or Initiation)
	- The proxy intercepts and decrypts TLS/SSL-encrypted traffic, examines the contents to ensure that it doesn't contain malicious code, then re-encrypts it before sending it on to the server
	- This only allow inspection of TLS/SSL-encrypted data before it reaches the server to prevent application layer attacks hidden inside

### Proxy and HTTPS

- HTTPS traffic (fortunately) cannot be read
- What if we WANT to read a client HTTPS traffic?
	- PRETEND to be the real server and be the termination of the SSL/TLS connection
- A possible solution is the **SSL bump**
	- It consists in using the requested host name to dynamically generate a server certificate and then impersonate the named server

It is a way to inspect https connection

- But: what if we **don't know the host name**?
	- Remember that we start the TLS handshake using an IP address and the hostname is sent in the HTTP request

### HTTPS certificate dilemma

- Virtual host: the same webserver can host multiple websites
	- According to the hostname field in the HTTP header, the server understands the requested website
- If HTTPS is used, the SSL/TLS connection requires a certificate to be sent by the server, but...
	- which certificate has to be sent?
		- A certificate valid for all the websites is out of discussion
		- The hostname is within the HTTP traffic but it is **encrypted**

### Server Name Indication

Server Name Indication (SNI) is an extension to TLS by
which a client indicates which hostname it is attempting
to connect to at **the start of the handshaking** process
	- It is in clear text
- TLS Extensions RFC:
	- https://tools.ietf.org/html/rfc3546#section-3.1
- This allows a server to present **multiple certificates** on the same IP address and TCP port number and hence allows
multiple secure (HTTPS) websites (or any other service over TLS) to be served by the same IP address **without requiring** 
all those sites to use the same certificate

### SNI example

![SNI image](https://d1smxttentwwqu.cloudfront.net/wp-content/uploads/2019/03/sni_virtual_hosting2.png)

### SNI implications

- An eavesdropper can see which site is being requested
- This helps security companied provide a filtering feature and governments implement censorship
	- Trick: [Domain Fronting](https://attack.mitre.org/techniques/T1090/004/) 
		- In the SNI use the name of a CDN server and then use a different host in the real HTTP request, always within the same CDN
- An upgrade called Encrypted SNI (ESNI) is being rolled out in an "experimental phase" to address this risk of domain eavesdropping

### SOCKS proxy

- Circuit level gateway
	- Works at the session layer of the OSI model, or as a "shim-layer" between the application layer and the transport layer of the TCP/IP stack
- Similar to the HTTP CONNECT proxy
	- A bit more versatile:
		- Many authentication mechanisms
		- Can tunnel TCP, but also UDP and IPv6(SOCKS5)
		- Can also work as a reverse proxy
- Implemented in SSH, putty and Tor


