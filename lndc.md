## Virtual Local Area Network (VLAN)

**Configuration:**
1. ``` S1# configure terminal ```
2. ``` S1(config)# vlan <vlan-number> ``` (99 for management)
3. ``` S1(config-vlan)# name <name> ```
7. ``` S1# exit ```
8. ``` S1(config)# interface <interface> ```
9. ```S1(config-if)# switchport mode access ```
10. ```S1(config-if)# switchport access vlan <number> ```
11. ```S1(config-if)# end ```
12. ``` S1# copy running-config startup-config ``` (save config to the startup configuration)

**Configure Trunk:** 
1. ``` Switch (config)# interface <interface> ```
2. ``` Switch (config-if)# switchport mode trunk ```
3. ``` Switch (config-if)# switchport trunk native vlan <number> ```

It is also possible to configure the trunk port so that only
a subset of VLANs is allowed: ``` Switch (config-if)# switchport trunk allowed vlan <example: 10-20> ```

**list all vlan configured:** ``` show vlan brief ```

**check the mode of an interface:** ``` show interface <interface> switchport ```

**assign ip address to switches:**(in case of management) 
1. ``` S1(config)# interface vlan 99 ```
2. ``` S1(config-if)# ip address <ip> <subnet mask> ```

**enable telnet on management swith:** 
1. ``` S1(config)# line vty 0 15 ```
2. ``` S1(config-line)# password <password> ```

**enable privileged mode(configurable):** ``` S1(config)# enable password esame ```

## Inter-VLAN Routing

**Connect VLAN 10 and VLAN 30:** (configuration on the port of router)

(assign ip addrress on interfaces first)
1. ``` S1(config)# interface Fa 0/6 ```
2. ``` S1(config-if)# switchport access vlan 10 ```
3. ``` S1(config)# interface Fa 0/5 ```
4. ``` S1(config-if)# switchport access vlan 30 ```

**Router-on-a-stick inter-vlan configuration:**

1. ``` R1(config)# interface Fa 0/0.10 ```
2. ``` R1(config-subif)# encapsulation dot1q 10 ```
3. ``` R1(config-subif)#ip address 172.17.10.1 255.255.255.0 ```
4. ``` R1(config)# interface Fa 0/0.30 ```
5. ``` R1(config-subif)# encapsulation dot1q 30 ```
6. ``` R1(config-subif)# ip address 172.17.30.1 255.255.255.0 ```
7. ``` R1(config)# interface Fa 0/0 ```
8. ``` R1(config-if)# no shutdown ```

the port connected to the router is configured in trunk mode:

9. ``` S1(config)# interface Fa 0/5 ```
10. ``` S1(config-if)# switchport mode trunk ```

## Access Control List (ACL)

### Standard ACL

**Permit Command:** ```Router(config)# access-list <number> permit <ip address> <optional wildcard>```

**Deny command:** ```Router(config)# access-list <number> deny <ip address> <optional wildcard>```

**Bound to interface:** 
```
Router(config)# interface <interface>
Router(config-if)# ip access-group <number> <in/out>
```

Special keyward instead of wildcards: any (255.255.255.255) and host(0.0.0.0)

**show ip interface** to verify ACL

### Extended ACL

**Command:** ``` Router(config) #access-list <access-list-number> {permit | deny} <protocol> <source address> <source mask> <destination address> <destination mask> <operator operand> established(Allow tcp to pass) ```

**Example:** ``` access-list 114 permit tcp 172.16.6.0 0.0.0.255 any eq telnet ```

if a generic filtering without protocol is needed, you can simply insert "ip".

**Association to interface is the same procedure as standard ACL**

### Named ACL

**Command:** ``Router(config)# ip access-list {extended|standard} <name> ``

**Configuration:** ``` Router(config-ext-nacl) #{permit | deny} <protocol> <source address> <source mask> <destination address> <destination mask> <operator operand> established(Allow tcp to pass) precedence <precedence> tos <tos> log time-range <time-range-name> ```

### Limiting the telnet access

Having configured an access list and a telnet password on the router the command is the following: `` access-class <acl-number> in ``

## Port Security

**Setup:** ```Switch(config-if)# switchport port-security ```

**Static**: ``switchport port-security mac-address <mac-address>``

**To look at Mac:** ```ipconfig /all```

**Dynamic:** ``Switch(config-if)# switchport port-security maximum 50 ``

**Sticky Dynamic:** ```Switch(config-if)# switchport port-security mac-address sticky ``` (everything is saved in the config file)

## Port Security violation

**to recover:** 
```
shutdown
no shutdown
```

**verify port security:** ``show port-security <address>``

## DHCP Security

**Step 1, Enable dhcp snooping globally:** ```S1(config)# ip dhcp snooping```

**Step 2, Enable dhcp snooping on selected VLANs (at least one):** ``S1(config)# ip dhcp snooping vlan vlan-id`` (if no vlan use for vlan 1)

**Step 3, Configure trusted interfaces:** ```S1(config-if)# ip dhcp snooping trust```

**Step 4, Disable option 82 insertion by switch**: ```S1(config)# no ip dhcp snooping information option```

## Port Mirroring

**Configuring local SPAN:**

1. ``` S1(config)# monitor session 1 source interface <interface 1> ```
2. ``` S1(config)# monitor session 1 destination interface <interface 2> ```

**Check SPAN sessions:** ``` show monitor ```

## Generic Routing Encapsulation

1. **Create a tunnel interface:** ``` R(config)# interface Tunnel <number> ```
2. **Configure GRE as the tunnel interface mode:** ``` R(config-if)# tunnel mode gre ip ```
3. **Assign an IP address to the tunnel interface:** ``` R(config-if)# ip address <ip> <subnet mask> ```
4. **Specify the tunnel source physical interface:** ``` R(config-if)# tunnel source <interface> ``` 
5. **Specify the tunnel destination IP address (IP address of the physical interface of the tunnel end point):** ``` R(config-if)# tunnel destination <ip> ```





