[Theme III - Virtualization](virtualization.md)		

[Theme IV - AutoScaling Mechanisms](autoscaling.md)

[Theme V - Cloud Storage](cloudstorage.md)

[Theme VI - Big Data Analytics](bigdata.md)
